
MetadataScraperChromePlugin

Download Selected Links
=======

Select links on a page and download them.
https://chrome.google.com/webstore/detail/autofillmagic3000/hnccjnjemjnmkecellcgbjfkllbaaimb?utm_source=chrome-app-launcher-info-dialog

[Zipfile](http://developer.chrome.com/extensions/examples/api/downloads/download_links.zip)

Content is licensed under the [Google BSD License](https://developers.google.com/open-source/licenses/bsd).

Calls
-----

* [downloads.download](https://developer.chrome.com/extensions/downloads#method-download)
* [extension.onRequest](https://developer.chrome.com/extensions/extension#event-onRequest)
* [extension.sendRequest](https://developer.chrome.com/extensions/extension#method-sendRequest)
* [tabs.executeScript](https://developer.chrome.com/extensions/tabs#method-executeScript)
* [tabs.query](https://developer.chrome.com/extensions/tabs#method-query)
* [windows.getCurrent](https://developer.chrome.com/extensions/windows#method-getCurrent)