var fields = ['aspect_submission_StepTransformer_field_sedici_creator_person',
              'aspect_submission_StepTransformer_field_dc_title',
              'aspect_submission_StepTransformer_field_sedici_title_subtitle',
              'aspect_submission_StepTransformer_field_mods_location',
              'aspect_submission_StepTransformer_field_dc_format_extent',
              'aspect_submission_StepTransformer_field_dc_description_abstract',
              'aspect_submission_StepTransformer_field_sedici_subject_keyword',
              'aspect_submission_StepTransformer_field_sedici_identifier_uri',
              'aspect_submission_StepTransformer_field_mods_originInfo_place',
              'aspect_submission_StepTransformer_field_dc_type',
              'aspect_submission_StepTransformer_field_sedici_identifier_issn',
              'aspect_submission_StepTransformer_field_sedici_relation_journalTitle',
              'aspect_submission_StepTransformer_field_sedici_relation_journalVolumeAndIssue',
              'aspect_submission_StepTransformer_field_dc_date_issued_year',
              'aspect_submission_StepTransformer_field_dc_date_issued_month']

var year = response.date_published.split('-')[0]
var month = response.date_published.split('-')[1]
console.log(response.document_type)
var values = [response.creator,
              response.title,
              response.title_subtitle,
              response.location,
              response.extent,
              response.abstract,
              response.keywords,
              response.identifier_uri,
              response.subject,
              response.document_type,
              response.issn,
              response.journal_title,
              response.volume_and_issue,
              year,
              month
              ]

for (var i = 0;i < fields.length; i++) {
  elem = document.getElementById(fields[i])
  if (elem && values[i]) elem.value = values[i]
}
