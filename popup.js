const API_URL = 'http://scraper.sedici.unlp.edu.ar/'

const months = ['enero','febrero','marzo','abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre']

// Download all visible checked links.
function downloadCheckedLinks() {
  var url = API_URL + '?pagina=' + $('#filter').val()
  $.get(url,function(response){

    chrome.windows.getCurrent(function (currentWindow) {
      chrome.tabs.query({active: true, windowId: currentWindow.id},
                        function(activeTabs) {
        chrome.tabs.executeScript(
          activeTabs[0].id, {code: `var response = ${JSON.stringify(response)}`, allFrames: true}, function() {
              chrome.tabs.executeScript(activeTabs[0].id, {file: 'filler.js', allFrames: true});
          });
      });
    });
  })
}

window.onload = function() {
  document.getElementById('fill_button').onclick = downloadCheckedLinks;
};
